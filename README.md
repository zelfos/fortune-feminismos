fortune-feminismos
============
Este repositorio es un clon de [BLAG-fortune](https://notabug.org/PangolinTurtle/BLAG-fortune) modificado para feminismos en lugar de anarquismo.

Requerimientos
------------
Sólo necesitas fortune mod. En sistemas basados en Debian:

```bash
sudo apt-get install fortune-mod
```

Instalación
------------
Para instalar ejecute:

```bash
sudo make install distro=debian
```

Para desinstalar ejecute:

```bash
sudo make uninstall distro=debian
```

La parte de distro=debian debe reemplazarse por la distro que usea. Por ejemplo, para Fedora sería distro=fedora.

Ver la lista de distros soportadas [aquí](DISTROS.md).

Uso
-------
Para correr el programa, sólo escribe en tu terminal:

```bash
fortune feminismos
```

Inicio de Bash/Zsh
-----------------------------
> Quiero ver fortunas cuando mi terminal inicia, como en BLAG 140k!

Un método sencillo sería usar el siguiente comando si usan bash:

```bash
echo -e "\nfortune -s feminismos\necho" >> ${XDG_CONFIG_HOME:-HOME}/.bashrc
```

O en zsh:

```bash
echo -e "\nfortune -s feminismos\necho" >> ${ZTODIR:-HOME}/.zshrc
```

Para deshabilitar las fortunes al inicio de la terminal, remueve estas líneas de $HOME/.bashrc o $HOME/.zshrc:

```bash
fortune -s anarchism
echo
```
