
SRC=feministas
INSTALLDIR=`sh distro.sh $distro`

all: feminismos.dat

feminismos.dat: feminismos
	strfile feminismos

feminismos: $SRC
	cat $SRC/* > feminismos

clean:V:
	rm -f feminismos.dat feminismos

install:V: all
	cp feminismos $INSTALLDIR
	cp feminismos.dat $INSTALLDIR

uninstall:V:
	rm $INSTALLDIR/feminismos
	rm $INSTALLDIR/feminismos.dat
