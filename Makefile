
SRC	= feministas
TARGET	= $(shell sh distro.sh $(distro))

all:		feminismos.dat

feminismos.dat:	feminismos
		@strfile feminismos

feminismos:	$(SRC)/*
		@cat $(SRC)/* > feminismos

clean:;		rm -f feminismos.dat feminismos

install:	all
		cp feminismos $(TARGET)
		cp feminismos.dat $(TARGET)

uninstall:;	rm $(TARGET)/feminismos
		rm $(TARGET)/feminismos.dat
